﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestingDemo.Factory;
using UnitTestingDemo.Models;

namespace UnitTestingDemo.UnitTests.NUnit
{
    [Category("NUnit Object Type Tests")]
    [TestFixture]
    public class ObjectTypeTests
    {
        [Test]
        public void ShouldCreateANormalPerson()
        {
            var sut = new PersonFactory();

            object person = sut.CreatePerson(false);

            Assert.That(person, Is.TypeOf<Person>());
        }


        [Test]
        public void ShouldCreateAGeek()
        {
            var sut = new PersonFactory();

            object person = sut.CreatePerson(true);

            Assert.That(person, Is.TypeOf<Geek>());
        }


        [Test]
        public void ShouldBeAGeekChildClass()
        {
            var sut = new PersonFactory();

            object noLife = sut.CreateNoLife();

            Assert.That(noLife, Is.InstanceOf<Geek>());
        }

        [Test]
        public void ShouldHaveTimePlayed()
        {
            var sut = new PersonFactory();

            object noLife = sut.CreateNoLife();

            Assert.That(noLife, Has.Property("TimePlayed"));
        }
    }
}
