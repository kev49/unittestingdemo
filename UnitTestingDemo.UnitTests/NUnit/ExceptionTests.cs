﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestingDemo.Business;

namespace UnitTestingDemo.UnitTests.NUnit
{
    [Category("NUnit Exception Tests")]
    [TestFixture]
    public class ExceptionTests
    {
        [Test]
        public void ShouldExceptionWhenDivideByZero()
        {
            var sut = new Calculator();

            Assert.That(() => sut.DivideInts(10, 0), Throws.Exception);
        }

        [Test]
        public void ShouldDivideByZeroExceptionWhenDivideByZero()
        {
            var sut = new Calculator();

            Assert.That(() => sut.DivideInts(10, 0), Throws.TypeOf<DivideByZeroException>());
        }
    }
}
