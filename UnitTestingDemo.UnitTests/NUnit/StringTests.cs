﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestingDemo.Models;

namespace UnitTestingDemo.UnitTests.NUnit
{
    [Category("NUnit String Tests")]
    [TestFixture]
    public class StringTests
    {
        [Test]
        public void ShouldReturnFullName()
        {
            var sut = new Person();

            var fullname = sut.GetFullName("John", "Doe");

            Assert.That(fullname, Is.EqualTo("John Doe"));
        }
    }
}
