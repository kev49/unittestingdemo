﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestingDemo.Models;

namespace UnitTestingDemo.UnitTests.NUnit
{
    [Category("NUnit Collection Tests")]
    [TestFixture]
    public class CollectionTests
    {
        [Test]
        public void ShouldHaveNoEmptyItems()
        {
            var sut = new Person();

            Assert.That(sut.Items, Is.All.Not.Empty);
        }


        [Test]
        public void ShouldHaveALaptop()
        {
            var sut = new Person();

            Assert.That(sut.Items, Contains.Item("Laptop"));
        }


        [Test]
        public void ShouldHaveTwoSmartphones()
        {
            var sut = new Person();

            Assert.That(sut.Items, Has.Exactly(2).ContainsSubstring("Smartphone"));
        }
    }
}
