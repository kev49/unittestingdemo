﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestingDemo.Models;

namespace UnitTestingDemo.UnitTests.NUnit
{
    [Category("NUnit Attribute Tests")]
    [TestFixture]
    public class AttributeTests
    {
        private SomeDisposableClass sut;

        [TestFixtureSetUp]
        public void SetupBeforeAnyTests()
        {
            Console.WriteLine("__________________________________________");
            Console.WriteLine("Setup before any tests");

            sut = new SomeDisposableClass();
        }

        [SetUp]
        public void SetupBeforeEachTest()
        {
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Setup before {0}", TestContext.CurrentContext.Test.Name);
        }

        [TearDown]
        public void TearDownAfterEachTest()
        {
            Console.WriteLine("Tear Down after {0}", TestContext.CurrentContext.Test.Name);
            Console.WriteLine("------------------------------------------");
        }



        [TestFixtureTearDown]
        public void TearDownAfterAnyTests()
        {
            Console.WriteLine("Tear Down after all tests");
            Console.WriteLine("__________________________________________");

            sut.Dispose();
        }


        [Test]
        public void ShouldDoSomething()
        {
            Console.WriteLine("Do something");
            Assert.That(sut.DoSomethingAndSayOk(), Is.EqualTo("Ok"));
        }

        [Test]
        public void ShouldDoSomethingAgain()
        {
            Console.WriteLine("Do something again");
            Assert.That(sut.DoSomethingAndSayOk(), Is.EqualTo("Ok"));
        }

        [Test]
        [Ignore]
        public void ShouldIgnoreThisTest()
        {
            Console.WriteLine("Ignore this test");
            Assert.That(sut.DoSomethingAndSayOk(), Is.EqualTo("Ok"));
        }


        [Test]
        [Repeat(2)]
        public void ShouldRepeatThisTest()
        {
            Console.WriteLine("Test repeated");
            Assert.That(sut.DoSomethingAndSayOk(), Is.EqualTo("Ok"));
        }
    }
}
