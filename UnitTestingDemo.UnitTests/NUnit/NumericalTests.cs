﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestingDemo.Business;

namespace UnitTestingDemo.UnitTests.NUnit
{
    [Category("NUnit Numerical Tests")]
    [TestFixture]
    public class NumericalTests
    {
        [Test]
        public void ShouldAddIntNumbers()
        {
            var sut = new Calculator();

            var result = sut.AddInts(5, 10);

            Assert.That(result, Is.EqualTo(15));
        }


        [Test]
        public void ShouldDivideInts()
        {
            var sut = new Calculator();

            var result = sut.DivideInts(10, 3);

            Assert.That(result, Is.EqualTo(3.3));
        }


        [Test]
        public void ShouldDivideIntsWithTolerance()
        {
            var sut = new Calculator();

            var result = sut.DivideInts(10, 3);

            Assert.That(result, Is.EqualTo(3.3).Within(0.1));
        }
    }
}
