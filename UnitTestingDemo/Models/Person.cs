﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo.Models
{
    public class Person
    {
        public List<string> Items { get; set; }

        public Person ()
        {
            CreateUsualItems();
        }


        public string GetFullName(string firstname, string lastname)
        {
            return firstname + " " + lastname;
        }


        public void CreateUsualItems()
        {
            Items = new List<string>()
            {
                "Laptop",
                "Smartphone one",
                "Watch",
                "Wristband one",
                "Smartphone two"
            };
        }
    }
}
