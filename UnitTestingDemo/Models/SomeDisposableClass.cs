﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo.Models
{
    public class SomeDisposableClass: IDisposable
    {
        public string DoSomethingAndSayOk()
        {
            // Do something

            return "Ok";
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
