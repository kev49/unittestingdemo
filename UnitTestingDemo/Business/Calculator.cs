﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo.Business
{
    public class Calculator
    {
        public int AddInts(int numberOne, int numberTwo)
        {
            return (numberOne + numberTwo);
        }

        public double AddDoubles(double numberOne, double numberTwo)
        {
            return (numberOne + numberTwo);
        }

        public double DivideInts(int number, int divider)
        {
            if (divider == 0)
            {
                throw new DivideByZeroException("Divider cannot be equal to 0");
            }

            return (double)number / divider;
        }

        public double MultiplyDoubles(double numberOne, double numberTwo)
        {
            return (numberOne * numberTwo);
        }
    }
}
