﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestingDemo.Models;

namespace UnitTestingDemo.Factory
{
    public class PersonFactory
    {
        public object CreatePerson(bool isGeek)
        {
            if (isGeek)
            {
                return new Geek();
            }

            return new Person();
        }

        public object CreateNoLife()
        {
            return new NoLife();
        }
    }
}
